<?php

// @codingStandardsIgnoreStart

namespace Drupal\retry_stream_wrapper;

/**
 * A trait to make stream wrappers retry their reads a little.
 */
trait StreamRetryTrait {

  /**
   * Max retries for file reads.
   *
   * @var int
   */
  protected $maxRetries = 10;

  /**
   * Sleep interval between file reads.
   *
   * @var int
   */
  protected $streamRetryInterval = 200000;

  /**
   * {@inheritdoc}
   */
  public function stream_open($uri, $mode, $options, &$opened_path) {
    if (!$this->fileModeIsRetrySupported($mode)) {
      return parent::stream_open($uri, $mode, $options, $opened_path);
    }

    $retries = 0;
    while ($retries++ < $this->maxRetries && !($handle = parent::stream_open($uri, $mode, $options, $opened_path))) {
      usleep($this->streamRetryInterval);
    }

    return $handle;
  }

  /**
   * Check if the mode is supported.
   *
   * Only support binary reads right now.
   *
   * @return bool
   *   A bool to check if the file mode is supported.
   */
  protected function fileModeIsRetrySupported($mode) {
    return $mode === 'rb' || $mode === 'br';
  }

}
