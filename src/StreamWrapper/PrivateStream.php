<?php

namespace Drupal\retry_stream_wrapper\StreamWrapper;

use Drupal\Core\StreamWrapper\PrivateStream as CorePrivateStream;
use Drupal\retry_stream_wrapper\StreamRetryTrait;

/**
 * Extends and replaces the core private stream wrapper.
 */
class PrivateStream extends CorePrivateStream {

  use StreamRetryTrait;

}
