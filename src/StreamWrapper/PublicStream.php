<?php

namespace Drupal\retry_stream_wrapper\StreamWrapper;

use Drupal\Core\StreamWrapper\PublicStream as CorePublicStream;
use Drupal\retry_stream_wrapper\StreamRetryTrait;

/**
 * Extends and replaces the core public stream wrapper.
 */
class PublicStream extends CorePublicStream {

  use StreamRetryTrait;

}
