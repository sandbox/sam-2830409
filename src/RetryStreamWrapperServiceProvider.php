<?php

namespace Drupal\retry_stream_wrapper;

use Drupal\Core\DependencyInjection\ContainerBuilder;
use Drupal\Core\DependencyInjection\ServiceProviderBase;

/**
 * Overrides the country_manager service.
 */
class RetryStreamWrapperServiceProvider extends ServiceProviderBase {

  /**
   * {@inheritdoc}
   */
  public function alter(ContainerBuilder $container) {
    if ($private_wrapper = $container->getDefinition('stream_wrapper.private')) {
      $private_wrapper->setClass('Drupal\retry_stream_wrapper\StreamWrapper\PrivateStream');
    }
    if ($public_wrapper = $container->getDefinition('stream_wrapper.public')) {
      $public_wrapper->setClass('Drupal\retry_stream_wrapper\StreamWrapper\PublicStream');
    }
  }

}
